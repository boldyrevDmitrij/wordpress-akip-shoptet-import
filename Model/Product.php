<?php

//namespace AkipShoptetImport\Model\Product;
class Product
{
    private $id;
    private $category_id;
    private $name;
    private $price;
    private $price_from;
    private $price_to;
    private $origin_url;
    private $description;
    private $short_description;
    private $images;
    private $variants;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCategoryId()
    {
        return $this->category_id;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getPriceFrom()
    {
        return $this->price_from;
    }

    public function getPriceTo()
    {
        return $this->price_to;
    }


    public function getOriginUrl()
    {
        return $this->origin_url;
    }

    public function getDescription()
    {
        return $this->description;
    }


    public function getShortDescription()
    {
        return $this->short_description;
    }


    public function getImages()
    {
        return $this->images;
    }

    public function getVariants()
    {
        return $this->variants;
    }





    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setCategoryId($categoryId)
    {
        $this->category_id = $categoryId;
        return $this;
    }

    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function setPriceFrom($priceFrom)
    {
        $this->price_from = $priceFrom;
        return $this;
    }

    public function setPriceTo($priceTo)
    {
        $this->price_to = $priceTo;
        return $this;
    }


    public function setOriginUrl($originUrl)
    {
        $this->origin_url = $originUrl;
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }


    public function setShortDescription($shortDescription)
    {
        $this->short_description = $shortDescription;
        return $this;
    }


    public function setImages($images)
    {
        $this->images = $images;
        return $this;
    }

    public function setVariants($variants)
    {
        $this->variants = $variants;
        return $this;
    }

    public function load($data)
    {
        if (isset($data['id'])) {
            $this->id = $data['id'];
        }
        $this->setName($data['name']);
        $this->setImgUrl($data['img_url']);
        $this->setShoptetDataUrl($data['img_url']);
        $this->setHeurekaCategory($data['heureka_category']);
        return $this;
    }
}
