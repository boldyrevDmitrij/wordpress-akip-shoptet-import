<?php
/**
 * Plugin name:     AkipShoptetImport
 * Description:     Import zboži ze Shoptetu
 * Version:         1.0
 * Author:          AKIP Solutions s.r.o.
 * Author URI:      https://akip.cz/
 * Test Domain:     akipshoptetimport
 * Domain Path:     /lang
 */
//namespace AkipShoptetImport\App;

// SECURITY REASON - check if wp is initialized
if (!function_exists('add_action')) {
    die();
}
require_once(dirname(__FILE__) . "/Model/Product.php");
$autoloader = dirname( __FILE__ ) . '/vendor/autoload.php';
if ( is_readable( $autoloader ) ) {
    require_once $autoloader;
}

// register global database
global $wpdb;
define("HTML_EMAIL_HEADERS", array('Content-Type: text/html; charset=UTF-8'));
use Automattic\WooCommerce\Client;

class AkipShoptetImport
{
    const PRODUCT_TALBE = 'akipshotetimport_product';

    const _URL = 'http://localhost:8099';
    const CONSUMER_KEY = 'ck_80db35c7ea72dce10348b5e7341b67e3dc3274a6';
    const CONSUMER_SECRET = 'cs_f0acfed809acac68171e9d0dad1a2e6ea4c48237';
    const IMPORT_START = 'Import zahájen';
    const IMPORT_END = 'Import ukončen';
    const IMPORT_PRODUCT_START = 'Zahájen import produktu - ';
    const IMPORT_PRODUCT_END = 'Ukončen import produktu - ';


    public static function getProductTable()
    {
        global $wpdb;
        return $wpdb->prefix . self::PRODUCT_TALBE;
    }

    public static function getPluginDir()
    {
        return (plugins_url('', __FILE__));
    }

    public static function writeToFile($file, $startMessage, DateTime $timeStamp, $data = []) {
        if (!$file) {
            return;
        }
        $text = "{$timeStamp->format('Y-d-m H:i:s')};{$startMessage}";
        if (!empty($data)) {
            foreach ($data as $key => $item) {
                if ($key === 0)
                    $text .= ';';
                if ($key === count($data))
                    $text .= $item;
                else
                    $text .= $item . ';';
            }
        }
        fwrite($file, $text . "\n");
    }

    public function register()
    {
        add_action('init', [$this, 'custom_post_type']);
        add_action('admin_menu', [$this, 'akipshoptetimport_show_nav_item']);
        add_action('admin_enqueue_scripts', [$this, 'akipshoptetimport_load_assets_admin']);
        add_action('wp_enqueue_scripts', [$this, 'akipshoptetimport_load_assets_front']);
        add_action('save_post', [$this, 'akipshoptetimport_save']);
    }

    static function activation()
    {
        flush_rewrite_rules();
    }

    static function deactivation()
    {
        flush_rewrite_rules();
    }

    public function akipshoptetimport_create_plugin_database_table()
    {
        global $wpdb;
        $wp_track_table = self::getProductTable();
        if ($wpdb->get_var("show tables like '$wp_track_table'") != $wp_track_table) {
            $sql = "CREATE TABLE `{$wp_track_table}` 
                    ( 
                        `id` INT NOT NULL AUTO_INCREMENT 
                        , `category_id` BIGINT UNSIGNED NOT NULL
                        , `name` VARCHAR(255) NOT NULL 
                        , `price` FLOAT NOT NULL 
                        , `price_from` FLOAT NULL 
                        , `price_to` FLOAT NULL 
                        , `origin_url` VARCHAR(500) NOT NULL 
                        , `description` LONGTEXT NULL 
                        , `short_description` LONGTEXT NULL 
                        , `images` LONGTEXT NULL 
                        , `variants` LONGTEXT NULL 
                        , PRIMARY KEY (`id`)
                    ) ENGINE = InnoDB";
            require_once(ABSPATH . '/wp-admin/includes/upgrade.php');
            dbDelta($sql);
        }
    }

    public function akipshoptetimport_show_nav_item()
    {
        add_menu_page(
            esc_html__('Produkty', 'akipshoptetimport'),
            esc_html__('Produkty', 'akipshoptetimport'),
            'manage_options',
            'shotetimport-options',
            [$this, 'akipshoptetimport_show_content'],
            'dashicons-products',
            81
        );
    }

    function akipshoptetimport_show_content()
    {
        include(dirname(__FILE__) . "/Model/Category.php");
        include(dirname(__FILE__) . "/Repository/CategoryRepository.php");
        include(dirname(__FILE__) . "/admin/view/" . 'list.php');
    }

    public function custom_post_type()
    {
        register_post_type('category', [
            'public' => true,
            'label' => esc_html__('Kategorie', 'akipshoptetimport'),
            'supports' => ['title', 'thumbnail'],
            'has_archive' => true,
            'menu_icon' => 'dashicons-list-view',
            'menu_position' => 80
        ]);
        add_action('add_meta_boxes', [$this, 'akipshoptetimport_metabox']);
    }

    public function akipshoptetimport_metabox()
    {
        add_meta_box(
            'akipshoptetimport_metabox',
            'Doplňující info',
            [$this, 'akipshoptetimport_render_metabox'],
            'category',
            'normal'
        );
    }

    public function akipshoptetimport_render_metabox($post)
    {
        $value = get_post_meta($post->ID, 'shoptet_url', true);
        $value2 = get_post_meta($post->ID, 'shoptet_category_settings', true);

        echo '<label for="akipshoptetimport_category_shoptet_url">Adresa XML feedu</label>';
        echo "<input type='text' id='akipshoptetimport_category_shoptet_url' name='shoptet_url' placeholder='vložte url adresu xml feedu' value='{$value}'>";
        echo "<br>";
        echo '<label for="akipshoptetimport_category_shoptet_category_settings">Název svátku (svátky oddělujte středníkem ";")</label>';
        echo "<input style='  margin-left: 25px;
  width: 50%;' type='text' id='akipshoptetimport_category_shoptet_category_settings' name='shoptet_category_settings' placeholder='vložte název svátku' value='{$value2}'>";
    }

    public function akipshoptetimport_save($post_id)
    {
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }
        $shotet_url = $_POST['shoptet_url'];
        update_post_meta($post_id, 'shoptet_url', $shotet_url);

        $shoptet_category_settings = $_POST['shoptet_category_settings'];
        update_post_meta($post_id, 'shoptet_category_settings', $shoptet_category_settings);
        if (file_get_contents($shotet_url)) {
            self::readXml($shotet_url, $post_id, $shoptet_category_settings);
        }
    }

    public static function createDateTimeWithTimeZone() {
        $date = new DateTime();
        $date->setTimezone(new DateTimeZone('Europe/Prague'));
        return $date;
    }

    public static function readXml($url, $post_id, $holidaysName = '')
    {
        global $wpdb;
        $woocommerce = new Client(
            self::_URL,
            self::CONSUMER_KEY,
            self::CONSUMER_SECRET,
            [
                'wp_api' => true,
                'version' => 'wc/v3',
                'timeout' => -1,
                'debug' => true
            ]
        );
        if ($holidaysName !== '' && strpos($holidaysName, ';')) {
            $holidaysName = explode(';', $holidaysName);
        }
        if (!is_array($holidaysName)) {
            $tmp[] = $holidaysName;
            $holidaysName = $tmp;
        }

        $post = get_post($post_id);
        $category = get_term_by( 'name', $post->post_title, 'product_cat' );
        if ($category->term_id) {
            $products = wc_get_products(array(
                'category' => array($category->slug),
                'posts_per_page' => -1 //select all products
            ));
            $productsId = array_map(function ($item) {
                return $item->id;
            }, $products);
            while (count($productsId) > 0 ) {
                $arr = array_splice($productsId, 0,100);
                $woocommerce->post('products/batch', [
                    'delete' => $arr
                ]);
            }
        } else {
            wp_insert_term( $post->post_title, 'product_cat', []);
            $category = get_term_by( 'name', $post->post_title, 'product_cat' );
        }
        $xml = simplexml_load_file($url);
        if ($xml) {
            $logFile = self::getLogFile();
            $startTime = self::createDateTimeWithTimeZone();
            self::writeToFile($logFile, self::IMPORT_START, $startTime, ['Kategorie - '.$post->post_title]);
            $counter = 1;
            $productsToImport = [];
            foreach ($xml as $item) {
                $continue = true;
                foreach ($item->INFORMATION_PARAMETERS->INFORMATION_PARAMETER as $infoParam) {
//                    exit(var_dump((string)$infoParam->NAME));
                    if ((string)$infoParam->NAME === 'Svátek' && in_array((string)$infoParam->VALUE, $holidaysName)) {
                        $continue = false;
                    }
                }
                if ($continue) {
                    continue;
                }
//                $productStartTime = self::createDateTimeWithTimeZone();
//                self::writeToFile($logFile, self::IMPORT_PRODUCT_START . (string)$item->NAME, $productStartTime);
                $categoryIds = [];
                $categoryIds[] = $category->term_id;
                foreach ($item->CATEGORIES->CATEGORY as $productCategory) {
                    $categories = explode(' > ', $productCategory);
                    $parent = '';
                    foreach ($categories as $key => $_category) {
                        if ($_category === $post->post_title) {
                            continue;
                        }
                        $existCategory = get_term_by( 'name', $_category, 'product_cat' );
                        if ($existCategory->term_id) {
                            $categoryIds[] = $existCategory->term_id;
                        } else {
                            if ($parent !== '') {
                                $parentCategory = get_term_by( 'name', $parent, 'product_cat' );
                                wp_insert_term( $_category, 'product_cat', ['parent' => $parentCategory->term_id]);
                            } else {
                                if ($key === 0) {
                                    wp_insert_term($_category, 'product_cat', []);
                                } else {
                                    $parent = $categories[$key-1];
                                    $parentCategory = get_term_by( 'name', $parent, 'product_cat' );
                                    wp_insert_term( $_category, 'product_cat', ['parent' => $parentCategory->term_id]);
                                }
                            }
                            $existCategory = get_term_by( 'name', $_category, 'product_cat' );
                            $categoryIds[] = $existCategory->term_id;
                        }
                        $parent = $_category;
                    }
                }
                $productCategories = [];
                $productCategories = array_map(function ($item) {
                    return ['id' => $item];
                }, $categoryIds);
                $product = [
                    'name'          => (string)$item->NAME,
                    'type'          => 'external',
                    'description'   => (string)$item->DESCRIPTION,
                    'short_description'   => (string)$item->SHORT_DESCRIPTION,
                    'external_url'  => (string)$item->ORIG_URL,
                    'categories'    =>
                        $productCategories
                    ,
                    'button_text'   => 'Otevřít E-shop'
                ];
                $images = $item->IMAGES->IMAGE;
                if ($images) {
                    if ($images->count() > 0) {
//                        define('ALLOW_UNFILTERED_UPLOADS', true);
//                        foreach ($images as $key => $image) {
//                            $product['images'][] = ['src' => (string)$image];
//                        }
                        $product['sku'] = (string)$images[0];
                    }
                }
                if (isset($item->VARIANTS) && $item->VARIANTS) {
                    $variants = $item->VARIANTS->VARIANT;
                    $priceArray = [];
                    $variantsHtml = "<br><table><thead><tr><th>Název</th><th>Cena</th></tr></thead><tbody>";
                    foreach ($variants as $variant) {
                        if ($variant->VISIBLE === false || (int)$variant->VISIBLE === 0) {
                            continue;
                        }
                        $parameters = $variant->PARAMETERS->PARAMETER;
                        if ($parameters->count() > 1) {
                            $parameters = $parameters->children()[0];
                        }
                        $priceArray[] = (float)$variant->PRICE_VAT;
                        $variantsHtml .= "<tr><td>{$parameters->NAME} {$parameters->VALUE}</td><td>{$variant->PRICE_VAT} Kč</td></tr>";
                    }
                    $variantsHtml .= "</tbody></table>";
                    $minPrice = min($priceArray);
                    $product['regular_price'] = (string)$minPrice;
                    $product['description'] =(string)$item->DESCRIPTION . $variantsHtml;
                } else {
                    $product['regular_price'] = (string)$item->PRICE_VAT;
                }
//                $woocommerce->post('products', $product);
//                $productEndTime = self::createDateTimeWithTimeZone();
//                $diff = $productStartTime->diff($productEndTime);
//                self::writeToFile($logFile, self::IMPORT_PRODUCT_END . (string)$item->NAME, $productEndTime, ["Import trval: {$diff->s} sekund", "Čítač: {$counter}"]);
                $counter++;
                $productsToImport[] = $product;
            }
            while (count($productsToImport) > 0 ) {
                $arr = array_splice($productsToImport, 0,100);
                $woocommerce->post('products/batch', [
                    'create' => $arr
                ]);
            }
            $endTime = self::createDateTimeWithTimeZone();
            $diff = $startTime->diff($endTime);
            self::writeToFile($logFile, self::IMPORT_END, $endTime, ["Import trval: {$diff->s} sekund","Prouktů: {$counter}"]);
            fclose($logFile);
        }
    }
    public static function getLogFile() {
        if (!file_exists(dirname(__FILE__) . '/logs')) {
            mkdir(dirname(__FILE__) . '/logs');
            $logFile = fopen(dirname(__FILE__) . '/logs/log.csv', 'w');
        }
        if (!file_exists(dirname(__FILE__) .'/logs/log.csv')) {
            $logFile = fopen(dirname(__FILE__) . '/logs/log.csv', 'w');
        } else {
            $logFile = fopen(dirname(__FILE__) . '/logs/log.csv', 'a+');
        }
        return $logFile;
    }

    // @email - Email address of the reciever
    // @subject - Subject of the email
    // @heading - Heading to place inside of the woocommerce template
    // @message - Body content (can be HTML)
    static function send_email_woocommerce_style($email, $subject, $heading, $message) {

        // Get woocommerce mailer from instance
        $mailer = WC()->mailer();

        // Wrap message using woocommerce html email template
        $wrapped_message = $mailer->wrap_message($heading, $message);

        // Create new WC_Email instance
        $wc_email = new WC_Email;

        // Style the wrapped message with woocommerce inline styles
        $html_message = $wc_email->style_inline($wrapped_message);

        // Send the email using wordpress mail function
        wp_mail( $email, $subject, $html_message, HTML_EMAIL_HEADERS );

    }

    // connect CSS & JS
    public function akipshoptetimport_load_assets_admin()
    {
        wp_enqueue_style('akipshoptetimportStyle', plugins_url('/assets/admin/style.css', __FILE__));
        wp_enqueue_script('akipshoptetimportScript', plugins_url('/assets/admin/main.js', __FILE__));
        wp_enqueue_script('jquery');
    }

    public function akipshoptetimport_load_assets_front()
    {
        wp_enqueue_style('akipshoptetimportStyle', plugins_url('/assets/front/style.css', __FILE__));
        wp_enqueue_script('akipshoptetimportScript', plugins_url('/assets/front/main.js', __FILE__));
    }
}

if (class_exists('AkipShoptetImport')) {
    $app = new AkipShoptetImport();
    $app->register();
}
register_activation_hook(__FILE__, [$app, 'activation']);
//register_activation_hook(__FILE__, [$app, 'akipshoptetimport_create_plugin_database_table']);
register_deactivation_hook(__FILE__, [$app, 'deactivation']);


