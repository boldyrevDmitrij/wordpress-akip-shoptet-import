<?php
if (isset($_GET['update'])) {
    $autoloader = plugins_url('vendor/autoload.php',  dirname(__DIR__));
    if ( is_readable( $autoloader ) ) {
        require_once $autoloader;
    }
    $posts = get_posts([
        'post_type' => 'category',
        'numberposts' => -1
        // 'order'    => 'ASC'
    ]);
    $woocommerce = new \Automattic\WooCommerce\Client(
        AkipShoptetImport::_URL,
        AkipShoptetImport::CONSUMER_KEY,
        AkipShoptetImport::CONSUMER_SECRET,
        [
            'wp_api' => true,
            'version' => 'wc/v3',
            'timeout' => 0
        ]
    );
    foreach ($posts as $item_) {
        $shotet_url = get_post_meta($item_->ID, 'shoptet_url')[0];
        if (file_get_contents($shotet_url)) {
            AkipShoptetImport::readXml($shotet_url, $item_->ID);
        }
    }
}
?>
<?php //echo plugins_url('vendor/autoload.php',  dirname(__DIR__))?>
<form method="get" action="/wp-admin/admin.php">
    <input type="hidden" name="page" value="shotetimport-options">
    <input type="hidden" name="update" value="true">
    <button type="submit">
        Obnovit všechny produkty
    </button>
</form>
