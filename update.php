<?php
echo 'xxx123';
require(__DIR__ . '/../../../wp-blog-header.php');
//require_once(__DIR__ . '/core.php');
$autoloader = 'vendor/autoload.php';
if ( is_readable( $autoloader ) ) {
    require_once $autoloader;
}
$posts = get_posts([
    'post_type' => 'category',
    'numberposts' => -1
    // 'order'    => 'ASC'
]);
$woocommerce = new \Automattic\WooCommerce\Client(
    AkipShoptetImport::_URL,
    AkipShoptetImport::CONSUMER_KEY,
    AkipShoptetImport::CONSUMER_SECRET,
    [
        'wp_api' => true,
        'version' => 'wc/v3',
        'timeout' => 0
    ]
);
foreach ($posts as $item_) {
    $shotet_url = get_post_meta($item_->ID, 'shoptet_url')[0];
    $holidaysName = get_post_meta($item_->ID, 'shoptet_category_settings')[0];
    if (file_get_contents($shotet_url)) {
        AkipShoptetImport::readXml($shotet_url, $item_->ID, $holidaysName);
    }
}
?>
